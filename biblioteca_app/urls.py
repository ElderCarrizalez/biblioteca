from django.urls import path
from biblioteca_app import views, datatables

urlpatterns = [
    path('', views.index, name='index'),
    path('category/', views.categories, name="categories"),
    path('category/datatables/', datatables.DTCategory.as_view(), name="DTCategory"),
    path('category/insert/', views.CategoryCreate.as_view(), name="CategoryCreate"),
    path('category/update/<int:pk>/', views.CategoryUpdate.as_view(), name="CategoryUpdate"),
    path('category/delete/<int:pk>/', views.CategoryDelete.as_view(), name="CategoryDelete"),

    path('books/', views.books, name="books"),
    path('books/datatables/', datatables.DTBook.as_view(), name="DTBook"),
    path('books/insert/', views.BookCreate.as_view(), name="BookCreate"),
    path('books/update/<int:pk>/', views.BookUpdate.as_view(), name="BookUpdate"),
    path('books/delete/<int:pk>/', views.BookDelete.as_view(), name="BookDelete"),
]