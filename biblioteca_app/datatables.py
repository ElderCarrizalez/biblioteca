from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models import Q

from biblioteca_app.models import Category, Book

class DTCategory(BaseDatatableView):
    columns = ['name']
    order_columns = ['name']

    def get_initial_queryset(self):
        category = Category.objects.all()
        return category

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(Q(name__icontains=search))
        return qs
     
    def prepare_results(self, qs):
        json_data = []
        for item in qs:
            try:
                json_data.append([
                    str(item.name),
                    item.id,
                    item.id,
                ])
            except:
                continue
        return json_data



class DTBook(BaseDatatableView):
    columns = ['tile', 'author']
    order_columns = ['title', 'author']

    def get_initial_queryset(self):
        book = Book.objects.all()
        return book

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(Q(title__icontains=search)|Q(author__icontains=search)|Q(category__name__icontains=search))
            
        
        return qs
     
    def prepare_results(self, qs):
        json_data = []
        for item in qs:
            try:
                categories = list(item.category.values_list('name', flat=True))
                json_data.append([
                    str(item.title),
                    str(item.author),
                    categories,
                    item.id,
                    item.id,
                ])
            except:
                continue
        return json_data