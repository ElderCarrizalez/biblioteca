from ast import Delete
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from biblioteca_app.models import Category, Book
from biblioteca_app.forms import BookForm, CategoryForm
# Create your views here.

def index(request):
    return render(request, 'index/index.html')

def categories(request):
    return render(request, 'categories/list.html')

class CategoryCreate(CreateView):
    template_name='categories/form.html'
    model = Category
    success_url = reverse_lazy('categories')
    form_class = CategoryForm
    success_message = "Categoría %(name)s creada correctamente."
    error_message = "Error al crear categoría."

class CategoryUpdate(UpdateView):
    template_name='categories/form.html'
    model = Category
    success_url = reverse_lazy('categories')
    form_class = CategoryForm
    success_message = "Categoría %(name)s actualizada correctamente."
    error_message = "Error al actualizar categoría."

class CategoryDelete(DeleteView):
    template_name='categories/confirm_delete.html'
    model = Category
    success_url = reverse_lazy('categories')
    success_message = "Categoría eliminada correctamente."
    error_message = "Error al eliminar categoría."



def books(request):
    return render(request, 'books/list.html')

class BookCreate(CreateView):
    template_name='books/form.html'
    model = Book
    success_url = reverse_lazy('books')
    form_class = BookForm
    success_message = "Libro %(title)s creado correctamente."
    error_message = "Error al crear libro."

class BookUpdate(UpdateView):
    template_name='books/form.html'
    model = Book
    success_url = reverse_lazy('books')
    form_class = BookForm
    success_message = "Libro %(title)s creado correctamente."
    error_message = "Error al crear libro."

class BookDelete(DeleteView):
    template_name='books/confirm_delete.html'
    model = Book
    success_url = reverse_lazy('books')
    form_class = BookForm
    success_message = "Libro eliminado correctamente."
    error_message = "Error al crear libro."

