from django.db import models

# Create your models here.

class Category(models.Model):
    name = models.CharField('Nombre', max_length=300)
    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField('Título', max_length=300)
    author = models.CharField('Autor', max_length=300)
    front_page = models.ImageField('Portada', upload_to='static/front_pages/')
    category = models.ManyToManyField(Category)